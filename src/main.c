#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <stdio.h>
#include <zephyr/sys/util.h>
#include <zephyr/drivers/i2c.h>
#include <errno.h>
#include <string.h>

#define LOG_LEVEL CONFIG_I2C_LOG_LEVEL
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(i2c_target);

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   1000

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#define NODE_I2C1 DT_NODELABEL(i2c1)
#define NODE_I2C3 DT_NODELABEL(i2c3)

#define TEST_DATA_SIZE	20

struct target_data {
	uint32_t buffer_size;
	uint8_t buffer[TEST_DATA_SIZE];
	uint32_t buffer_idx;
	bool first_write;
};

struct target_data data = {
	.buffer_size = TEST_DATA_SIZE,
	.buffer = {0},
	.buffer_idx = 0,
	.first_write = false
};

char eeprom[TEST_DATA_SIZE] = "i2c-target-demo-lava";

static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

static int target_write_requested(struct i2c_target_config *config)
{
	LOG_DBG("target_write_requested\n");

	data.first_write = true;

	return 0;
}

static int target_read_requested(struct i2c_target_config *config, uint8_t *val)
{
	LOG_DBG("target_read_requested\n");
	*val = data.buffer[data.buffer_idx];
	return 0;
}

static int target_write_received(struct i2c_target_config *config, uint8_t val)
{
	LOG_DBG("target_write_received\n");
	if (data.first_write) {
		data.buffer_idx = val;
		data.first_write = false;
	} else {
		data.buffer[data.buffer_idx++] = val;
	}

	data.buffer_idx = data.buffer_idx % data.buffer_size;
	return 0;
}

static int target_read_processed(struct i2c_target_config *config, uint8_t *val)
{
	LOG_DBG("target_read_processed\n");
	data.buffer_idx = (data.buffer_idx + 1) % data.buffer_size;

	*val = data.buffer[data.buffer_idx];
	return 0;
}

static int target_stop(struct i2c_target_config *config)
{
	LOG_DBG("target_stop\n");
	data.first_write = true;
	return 0;
}

static const struct i2c_target_callbacks target_callbacks = {
	.write_requested = target_write_requested,
	.read_requested = target_read_requested,
	.write_received = target_write_received,
	.read_processed = target_read_processed,
	.stop = target_stop,
};


int main(void)
{
	uint8_t who_am_i = 4;
	int ret;
	const struct device *const i2c1 = DEVICE_DT_GET(NODE_I2C1);
	const struct device *const i2c3 = DEVICE_DT_GET(NODE_I2C3);
	struct i2c_target_config cfg;

	//set i2c target address for the first time
	cfg.address = 0x54;
	cfg.callbacks = &target_callbacks;

	if(i2c1 == NULL || !device_is_ready(i2c1))
	{
		printf("could not get i2c");
		return 0;
	}

	ret = i2c_target_register(i2c1, &cfg);
	if (ret)
	{
		printf("unable to set eeprom on i2c1: %d\n", ret);
	}

	if (!gpio_is_ready_dt(&led)) {
		return 0;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return 0;
	}

/*
Reserved Addresses
The I2C specification has reserved two sets of eight addresses, 1111XXX and 0000XXX.
These addresses are used for special purposes. 
The following table has been taken from the I2C Specifications (2000).

Slave Address	R/W Bit	Description
000 0000	0	General call address
000 0000	1	START byte
000 0001	X	CBUS address
000 0010	X	Reserved for different bus format
000 0011	X	Reserved for future purposes
000 01XX	X	Hs-mode master code
111 10XX	X	10-bit slave addressing
111 11XX	X	Reserved for future purposes

No device is allowed to acknowledge at the reception of the START byte.

STM32F4 seems to behave normally from address 0x00-0x08 however it is inconsistant
from 0x78 and onwards probably because it sees it as 10bit bus indicator.
*/


	for(uint8_t j = 0x08; j <= 0x77; j++)
	{
		// Unregister i2c Target device
		ret = i2c_target_unregister(i2c1, &cfg);

		// Clear Buffer
		memset(&data.buffer[0], 0, sizeof(data.buffer));

		// Set new address	
		cfg.address = j;
		
		// re-register i2c target device with new address
		ret = i2c_target_register(i2c1, &cfg);
		if (ret)
		{
			printf("unable to set eeprom on i2c1 %hhX: %d\n", j, ret);
			break;
		}

		// Write data to i2c target
		for(uint8_t i = 0; i < TEST_DATA_SIZE; i++)
		{
			ret = i2c_reg_write_byte(i2c3, j, i, eeprom[i]);
			if (ret)
			{
				printf("unable to write to 0x%hhX 0x%hhX: %d\n", j, i, ret);
				return 0;
			}
			printf("wrote 0x%hhX 0x%hhX: %c\n", j, i, eeprom[i]);
			k_msleep(20);
		}

		// Read data from i2c target 
		for(uint8_t i = 0; i < TEST_DATA_SIZE; i++)
		{
			ret = i2c_reg_read_byte(i2c3, j, i, &who_am_i);
			if (ret)
			{
				printf("unable to read from 0x%hhX 0x%hhX: %d\n", j, i, ret);
				return 0;
			}
			printf("read 0x%hhX 0x%hhX: %c\n", j, i, who_am_i);
			k_msleep(20);
		}
	}

	while (1) {
		ret = gpio_pin_toggle_dt(&led);
		if (ret < 0) {
			return 0;
		}
		k_msleep(SLEEP_TIME_MS);
	}
	return 0;
}
